.PHONY: all html

all: build build/favicon.svg build/main.css build/mast.jpg html

build:
	mkdir build

clean: build
	rm -r build/*.html

html:
	./generate-html.sh

build/favicon.svg: build
	ln -s ../favicon.svg build/favicon.svg	

build/main.css: build
	ln -s ../main.css build/main.css

build/mast.jpg: build
	ln -s ../mast.jpg build/mast.jpg
