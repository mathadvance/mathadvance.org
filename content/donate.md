---
title: Donate to Math Advance
desc: Instructions for sending a donation to Math Advance.
---

# Donate

## Payment Methods

There are a couple of ways we can take payment. We strongly prefer PayPal and Venmo.

If you are donating with PayPal, please "send as friend" so we don't get charged with a transaction fee.

- PayPal ([paypal.me/matournament](https://paypal.me/matournament))
- Venmo ([\@mathadvance](https://account.venmo.com/u/mathadvance))

## What do your donations go towards?

Your donations have a real impact on Math Advance! 

- Our [math contests](https://mat.mathadvance.org) have no entry fee, and we are able to offer generous prize pools.
- Donations also cover system administration costs ($120 a year to run our servers, $10 a year for domain). 

We currently have around $1,000. As such, we are financially in the green and do not need to immediately engage in any fundraising activities. However, your continued support is the only reason we can keep offering our services for free.
