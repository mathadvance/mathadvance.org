---
title: Math Advance
desc: Math Advance is a 501(c)(3) non-profit that enriches the mathematical development of gifted students.
---

<style>
html {
	color: var(--dark-fg);
	background-color: var(--dark-bg);
	background-image: url('mast.jpg');
	background-size: cover;
	
    a.nav {
	    color: var(--dark-fg);
    }

    a.nav:visited {
	    color: var(--dark-fg);
    }
}
</style>

# Homepage

*Thanks to sparkles for the background image!*

We're a group of undergraduate students enthusiastic about teaching and writing math.

Our most notable projects are our [MAT contest series](https://mat.mathadvance.org) and our AIME class [MAST](https://mast.mathadvance.org).

## People

Everyone who has worked with Math Advance before is listed here, even if they are no longer working with us. If I have forgotten anyone, I apologize --- please let me know and I'll add you to the list!

- Taiki Aiba
- Aditya Chandrasekhar
- Dennis Chen
- Eric Chen
- Isaac Chen
- William Dai
- Raymond Feng
- Abrar Fiaz
- Aaron Guo
- Ethan Han
- Pranav Konda
- Aarush Khare
- Ethan Liu
- Maggie Liu
- Amol Rama
- Kiran Reddy
- Aprameya Tripathy
- Prajith Velicheti
- Jerry Xiao
- Jeffrey Xu
- Robert Yang
- Dylan Yu
- Andrew Yuan
- Brian Zhang
- William Zhao
- Aaron Zhou
- Kelin Zhu
