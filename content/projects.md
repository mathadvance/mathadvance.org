---
title: Projects
desc: Math Advance's projects, most notably MAT, MAST, and mapm.
---

# MAT

[MAT](https://mat.mathadvance.org) is our flagship contest series. The Summer MAT has 3 sets of 3 problems, each set to be done in 30 minutes, along with a tiebreaker set - so in total, 4 sets of 3. The difficulty ranges from mid-AMC 10 to late AIME, with most of the difficulty variation occurring within a set and less of it between sets (though there is an increase between sets). It is typically held in the summer.

An easier version of the MAT is held in the winter. The Winter MAT also has 3 sets of 3 problems, but it does not have a tiebreaker and the time limit for each set is 20 minutes. 

## mapm

[mapm](https://mapm.mathadvance.org) is the software which Math Advance uses to create its contests. The principle purpose of mapm is to modularize problems into individual files which allows them to be programatically composed into a contest. My personal website contains a [demonstration](https://dennisc.net/writing/tech/why-mapm) of the problem mapm was designed to fix. The [Math Advance mapm contest archives](https://gitlab.com/mathadvance/contest-releases) may be of interest.

Of course, mapm is open source. It is licensed with [3-Clause BSD](https://opensource.org/licenses/BSD-3-Clause).

# Teaching

This fall, we are teaching [an AIME course](https://dennisc.net/writing/blog/mast-returns). [Applications](https://dennisc.net/apply-2023.pdf) are due September 3rd, and classes tentatively begin the Sunday after. Pricing is $100 for a semester of 10 lectures, plus potential guest lectures.
